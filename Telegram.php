<?php


namespace PostmanBot;


defined('_EXEC') or exit();

class Telegram
{
    public $token = '';
    public $result = [];

    /**
     * @param array $payload
     * @param $method
     * @return false|array
     */
    public function sendToTelegram(array $payload, $method)
    {
        $json = json_encode($payload);
        $ch = curl_init('https://api.telegram.org/bot' . $this->token . '/' . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $this->result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if (isset($this->result['ok']) && $this->result['ok']) return $this->result;
        return false;
    }

    //  ASSOC_ARRAY ( 'markup_reply' => JSON )
    // public function sendToTelegram_($params, $method)
    // {
    //     $ch = curl_init('https://api.telegram.org/bot' . $this->token . '/' . $method);
    //     curl_setopt($ch, CURLOPT_POST, 1);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_HEADER, false);
    //     $result = curl_exec($ch);
    //     curl_close($ch);
    //
    //     return $result;
    // }

    /***
     * @param array $payload
     * @return false|array
     **
     * $payload = [
     *     'chat_id' => $chat_id,
     *     'message_id' => (int)$message_id,
     * ];
     */
    public function deleteMessage($payload, $chat_id = null)
    {
        return $this->sendToTelegram($payload, 'deleteMessage');
    }


    /**
     * @param $payload
     * @return false|array
     * *
     * $payload = [
     *     'chat_id' => $chat_id,
     *     'message_id' => (int)$message_id,
     *     'text' => $new_data,
     * ];
     */
    public function editMessageText($payload)
    {
        return $this->sendToTelegram($payload, 'editMessageText');
    }


    /**
     * @param $payload
     * @return false|array
     * *
     * $payload = [
     *     'chat_id' => $chat_id,
     *     'message_id' => (int)$message_id,
     *     '...' => $new_data,
     * ];
     */
    public function editMessage($payload)
    {
        return $this->sendToTelegram($payload, 'editMessage');
    }


    /**
     * @param $payload
     * @return false|array
     **
     * $payload = [
     *      'chat_id' => $params['chat_id'],
     *      'message_id' => $params['message_id'],
     *      'reply_markup' => $params['reply_markup'],
     * ];
     */
    public function editMessageReplyMarkup($payload)
    {
        return $this->sendToTelegram($payload, 'editMessageReplyMarkup');
    }


    /**
     * @param array $message_data
     * @param array $media
     * @return string|null
     */
    static public function getMedia(array $message_data, &$media = [])
    {
        $method = null;
        if (isset($message_data['text'])) {
            $method = 'sendMessage';
            $media['text'] = $message_data['text'];
        } elseif (isset($message_data['photo'])) {
            $method = 'sendPhoto';
            $media['photo'] = $message_data['photo'][0]['file_id'];
        } elseif (isset($message_data['audio'])) {
            $method = 'sendAudio';
            $media['audio'] = $message_data['audio']['file_id'];
        } elseif (isset($message_data['video'])) {
            $method = 'sendVideo';
            $media['video'] = $message_data['video']['file_id'];
        } elseif (isset($message_data['animation'])) {
            $method = 'sendAnimation';
            $media['animation'] = $message_data['animation']['file_id'];
        } elseif (isset($message_data['voice'])) {
            $method = 'sendVoice';
            $media['voice'] = $message_data['voice']['file_id'];
        } elseif (isset($message_data['sticker'])) {
            $method = 'sendSticker';
            $media['sticker'] = $message_data['sticker']['file_id'];
        } elseif (isset($message_data['venue'])) {
            $method = 'sendVenue';
            $media = [
                'latitude' => $message_data['venue']['location']['latitude'],
                'longitude' => $message_data['location']['longitude'],
                'title' => $message_data['venue']['title'],
                'address' => $message_data['venue']['address'],
            ];
        } elseif (isset($message_data['location'])) {
            $method = 'sendLocation';
            $media = [
                'latitude' => $message_data['location']['latitude'],
                'longitude' => $message_data['location']['longitude']
            ];

        } elseif (isset($message_data['video_note'])) {  // todo test
            $method = 'sendVideoNote';
            $media['video_note'] = $message_data['video_note']['file_id'];
        } elseif (isset($message_data['media'])) {  // todo test
            //media	= [Array of InputMediaPhoto and InputMediaVideo ]
            //A JSON-serialized array describing photos and videos to be sent, must include 2-10 items
            $method = 'sendMediaGroup';
            $media['media'] = $message_data['media'];

        } elseif (isset($message_data['document'])) {
            $method = 'sendDocument';
            $media['document'] = $message_data['document']['file_id'];
        }

        if (isset($message_data['caption'])) {
            $media['caption'] = $message_data['caption'];
        }

        return $media ? $method : null;
    }


    /**
     * @param array $message_data
     * @return string|null
     */
    static public function getMethod(array $message_data)
    {
        return self::getMedia($message_data);
    }


    public function storeMessage($post_msg_json, $storage_channel)
    {
        $payload = [];
        $method = self::getMedia(json_decode($post_msg_json, true), $payload);

        $payload['chat_id'] = $storage_channel;

        $result = self::sendToTelegram($payload, $method);

        if ($result)
            return $result['result']['message_id'];

        return null;
    }


    public function send(array $params)
    {
        if (! $params) return null;
        if (! isset($params['method'])) $params['method'] = 'sendMessage';

        $payload['chat_id'] = $params['chat_id'];
        foreach ($params['payload'] as $key => $val) {
            $payload["$key"] = $val;
        }

        if (isset($params['buttons'])) {
            $payload['reply_markup'] = [
                'keyboard' => $params['buttons'],
                'one_time_keyboard' => false,
                "resize_keyboard" => true,
            ];

        } elseif (isset($params['inline_keyboard'])) {
            $payload['reply_markup'] = [
                'inline_keyboard' => $params['inline_keyboard'],
            ];
        } elseif (isset($params['force_reply'])) {
            $payload['reply_markup']['force_reply'] = $params['force_reply'];
        }
        $payload['reply_markup']['selective'] = true;

        if (isset($params['reply_to_message_id'])) {
            $payload['reply_to_message_id'] = $params['reply_to_message_id'];
        }

        return $this->sendToTelegram($payload, $params['method']);
    }
}