<?php


namespace PostmanBot;


defined('_EXEC') or exit();

trait IngdgApi
{
    private static $urlGetUserBalance = 'https://wf.ingdg.com/ExternalClients/getBalancefromTelegram/';
    private static $urlGetUserBalanceHistory = 'https://wf.ingdg.com/ExternalClients/getInvoiceByTlgid/';
    private static $urlGetUserId = 'https://wf.ingdg.com/ExternalClients/fromTelegram/';
    private static $urlGetUserReferral = 'https://wf.ingdg.com/ExternalClients/getMyReferal/';
    private static $urlGetInvoice = 'https://wf.ingdg.com/ExternalClients/getInvoiceAndReferenceToPayment/';
    private static $urlGetInvoiceInfo = 'https://wf.ingdg.com/ExternalClients/getInvoiceData/';
    private static $urlGetGroups = 'https://datahunter.xyz/getdata/tlgcli/mailing/channeldata/';
    private static $urlSetMailingQueue = 'https://api.ingdg.com/apidev/tlg/postman/queue/in/bigpostman/';
    private static $urlGetMailingQueueMessages = 'https://api.ingdg.com/apidev/tlg/postman/queue/count/';
    private static $urlReleaseHold = 'https://wf.ingdg.com/ExternalClients/getInvoiceHoldDelete/';
    private static $urlCommitPayment = 'https://wf.ingdg.com/ExternalClients/getInvoiceHoldPay/';

    private static $userReferralUrlTemplate = 'https://t.me/BigPostman_bot?start=';


    static private function request($url, $params = '', $post = false)
    {
        if ($post) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, '', '&'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            if (is_array($params)) {
                $request = $url . '?' . http_build_query($params, '', '&');
            } else {
                $request = $url;
            }

            $result = file_get_contents($request);
        }

        // if ($result && array_key_exists('ok', $result) && $result['ok']) {
        //     return $result;
        // } else {
        //     return false;
        // }

        return json_decode($result, true);

    }


    static public function getInvoice($params)
    {
        $result = self::request(self::$urlGetInvoice, $params);
        if ($result && array_key_exists('ok', $result) && $result['ok']) {
            return $result;
        }
        return false;
    }


    static public function getInvoiceInfo($invoice_id)
    {
        $params['invoice_id'] = $invoice_id;
        $result = self::request(self::$urlGetInvoiceInfo, $params);
        if ($result && ! (array_key_exists('result', $result) && $result['result'] == 'error')) {
            return $result;
        }
        return false;
    }


    static public function commitPayment($invoice_id)
    {
        if ($invoice_id) {
            $params['invoice_id'] = $invoice_id;
            $result = self::request(self::$urlCommitPayment, $params);
            if ($result && array_key_exists('ok', $result) && $result['ok']) {
                return $result;
            }
        } elseif ($invoice_id == 0) {
            return true;
        }
        return false;
    }


    static public function releaseHold($invoice_id)
    {
        $params['invoice_id'] = $invoice_id;
        $result = self::request(self::$urlReleaseHold, $params);
        if ($result && ! (array_key_exists('ok', $result) && $result['ok'])) {
            return $result;
        }
        return false;
    }


    static public function getGroups()
    {
        $result = self::request(self::$urlGetGroups);

        if ($result && array_key_exists('group', $result)) {
            return $result['group'];
        }
        return false;
    }


    static public function getUserId($user_lg_id, $user_data, $referral_id)
    {
        $params = [
            'tlg_id' => $user_lg_id,
            'client_info' => $user_data
        ];
        if ($referral_id)
            $params['refer'] = $referral_id;

        $result = self::request(self::$urlGetUserId, $params);

        if ($result && isset($result['id'])) {
            return $result['id'];
        }
        return false;
    }


    static public function getUserBalance($user_tg_id)
    {
        $params['tlg_id'] = $user_tg_id;

        $result = self::request(self::$urlGetUserBalance, $params);

        if ($result && array_key_exists('total', $result)) {
            if (! array_key_exists('hold', $result))
                $result['hold'] = 0;
            $result['ok'] = true;
            // todo del
            //  return ['ok' => true, 'total' => 10000, 'hold' => 1000];
            return $result;
        } else {
            return ['ok' => false, 'total' => -999999, 'hold' => -999999];
        }
    }


    static public function getUserBalanceHistory($user_tg_id, $status = 'fully_paid')
    {
        $params['tlg_id'] = $user_tg_id;
        $params['status'] = $status;
        $params['limit'] = 10;
        $params['orderBY'] = 'ASC';

        $result = self::request(self::$urlGetUserBalanceHistory, $params);
        if ($result && array_key_exists('ok', $result) && ! $result['ok']) {
            return $result['invoices'];
        }
        return [];
    }


    static public function getUserReferral($user_lg_id)
    {
        $params = [
            'tlg_id' => $user_lg_id,
        ];

        $result = self::request(self::$urlGetUserReferral, $params);

        if ($result && array_key_exists('ok', $result) && $result['ok']) {
            return $result;
        }
        return ['count' => '-', 'sum' => '-'];
    }


    static public function getUserReferralUrl($user_lg_id)
    {
        $params = [
            'tlg_id' => $user_lg_id,
        ];

        $result = self::request(self::$urlGetUserId, $params);

        if ($result && array_key_exists('id', $result)) {
            return self::$userReferralUrlTemplate . $result['id'];
        }
        return '';
    }


    public static function sendToMailingQueue(&$mailing_request, $post_json)
    {
        $params = [
            'name_queue' => $mailing_request->mailing_group_key,
            'uid' => $mailing_request->user_id,
            'callback' => CALLBACK_URL . '&request_id=' . $mailing_request->id,
            'message_id' => $mailing_request->stored_message_id,
            'param_queue' => ['data' => $post_json]
        ];

        // $result = ['ok' => 1, 'queue_name' => 'test_01'];
        $result = self::request(self::$urlSetMailingQueue, $params, true);

        if ($result && array_key_exists('ok', $result) && $result['ok']) {
            // {"ok":true,"queue_name":"pr_100","count_channels":2}
            return $result['queue_name'];
        }
        return false;
    }


    public static function isMailingQueueEmpty($mailing_request_id)
    {
        $result = self::request(self::$urlGetMailingQueueMessages . $mailing_request_id . '/');
        // $result = {"ok":true,"count":1};
        if ($result && array_key_exists('ok', $result) && $result['ok']) {
            if (array_key_exists('count', $result) && ! $result['count']) {
                return true;
            }
        }
        return false;
    }
}

