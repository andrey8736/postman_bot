<?php


namespace PostmanBot;


class BaseModel
{
    protected $db = null;
    protected $db_prefix = DB_PREFIX;


    public function __construct()
    {
        $this->db = DataBase::getInstance();

        $this->db->init();

    }


    protected function tablePrefix($table_name)
    {
        return $this->db_prefix . $table_name;
    }


}