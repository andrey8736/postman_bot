<?php

namespace PostmanBot;


use PDO;
use Postmanbot\Traits\Singleton;


defined('_EXEC') or exit();


class DataBase extends Singleton
{

    private $pdo;

    final public function init()
    {
        if ($this->pdo) return null;

        $db_host = DB_HOST;
        $db_name = DB_NAME;
        $db_user = DB_USER;
        $db_password = DB_PASSWORD;

        if (! ($db_host && $db_name && $db_user && $db_password)) return true;

        $this->pdo = new PDO(
            'mysql:host=' . $db_host . ';dbname=' . $db_name,
            $db_user,
            $db_password
        );
        $this->pdo->exec('SET NAMES UTF8');

        return false;
    }


    public function query($sql, array $options = [], $className = 'stdClass')
    {
        if (! $this->pdo) return null;
        $sth = $this->pdo->prepare($sql);
        $result = $sth->execute($options);

        if (false === $result) {
            return null;
        }

        return $sth->fetchAll();
        // return $sth->fetchAll(\PDO::FETCH_CLASS, $className);
    }


    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }


    public function prepare($sql)
    {
        return $this->pdo->prepare($sql);
    }


    public function execute($params = [])
    {
        return $this->pdo->execute($params);
    }
}
