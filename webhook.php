<?php

define('_EXEC', 'web_hook');

include('config.php');

use PostmanBot\Menu;
use PostmanBot\Message;
use PostmanBot\Post;
use PostmanBot\Render;
use PostmanBot\User;


if (isset($_GET['TOKEN'])) {
    if ($_GET['TOKEN'] != TOKEN) {
        http_response_code(404);
        exit();
    }
} else {
    http_response_code(404);
    exit();
}


if (isset($_GET['test'])) {
    $data = file_get_contents('hook_request.json');
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
} else {
    $data = file_get_contents('php://input');
    file_put_contents('hook_request.json', $data);
}

$data_json = json_decode($data, true);
if (array_key_exists('channel_post', $data_json)) {
    file_put_contents('./channel_post/' . $data_json['channel_post']['chat']['id'] . '.json', $data);
}

$tg_message = new Message(BOT_TOKEN, $data);
dbg("DATA", $data_json, "tg_message", $tg_message);


if (! $tg_message->chat_id) exit();
if ($tg_message->from_is_bot) exit();

if ($tg_message->user_tg_id) {
    // Check Referral
    $referral_id = null;
    if ($tg_message->message_array && isset($tg_message->message_array['text'])) {
        preg_match_all("/^\/start\s*(\d*)$/", $tg_message->message_array['text'], $matches);
        if ($matches[1]) {
            $referral_id = (int)$matches[1][0];
        }
    }
    $usr = new User($tg_message->user_tg_id, json_encode($tg_message->from_array), $referral_id);
} else {
    exit();
}

dbg("USER", $usr);

$new_payload = [];

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Callback
//
if ($tg_message->callback_data) {
    $action = '';
    $callback_data = explode('#', $tg_message->callback_data);
    if ($callback_data) {
        if (array_key_exists(1, $callback_data))
            $action = $callback_data[1];
    }
    dbg("Callback Message. Action: $action", "Callback array.", $callback_data);

    switch ($action) {
        case 'showPostList':
            dbg("Render::showPostList");
            Render::showPostList($usr, $tg_message, $callback_data);
            break;
        case 'deletedraft':
            dbg("Render::deleteDraft: " . $tg_message->reply_to['message_id'] . " " . $tg_message->message_id);
            Render::deleteDraft($usr, $tg_message);
            break;
        case 'savedraft':           // ok
            dbg("Render::saveDraft $tg_message->message_id" . " " . $tg_message->reply_to['message_id']);
            Render::saveDraft($usr, $tg_message);
            break;
        case 'deletePost':          // ok
            dbg("Render::deletePost");
            Render::deletePost($usr, $tg_message, $callback_data);
            break;
        case 'editPost':        // ok
            dbg("Render::editPost");
            Render::editPost($usr, $tg_message, $callback_data);
            break;
        case 'setPostMenu':
            dbg("Render::setPostMenu");
            Render::setPostMenu($usr, $tg_message, $callback_data);
            break;
        case 'setPostMenuPosting':
            dbg("Render::setPostMenuPosting");
            Render::setPostMenuPosting($usr, $tg_message, $callback_data);
            break;
        case 'setPostMenuPayment':
            dbg("Render::setPostMenuPayment");
            Render::setPostMenuPayment($usr, $tg_message, $callback_data);
            break;
        case 'doPayment':
            dbg("Render::doPayment");
            Render::doPayment($usr, $tg_message, $callback_data);
            break;
        case 'setUserBalanceMenuPayment':
            dbg("Render::setUserBalanceMenuPayment");
            Render::setUserBalanceMenuPayment($usr, $tg_message, $callback_data);
            break;
        case 'showBalanceHistory':
            dbg("Render::showBalanceHistory");
            Render::showBalanceHistory($usr, $tg_message, $callback_data);
            break;
        // case 'rechargeBalance':
        //     dbg("Render::rechargeBalance");
        //     Render::rechargeBalance($usr, $tg_message, $callback_data);
        //     break;
        case 'getInvoiceInfoRequest':
            dbg("Render::getInvoiceInfoRequest");
            Render::getInvoiceInfoRequest($usr, $tg_message, $callback_data);
            break;
        case 'showMailingRequestList':
            dbg("Render::showMailingRequestList");
            Render::showMailingRequestList($usr, $tg_message, $callback_data);
            break;
        case 'showHelp':
            dbg("Render::showHelp");
            Render::showHelp($usr, $tg_message, $callback_data);
            break;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reply
    //
} elseif (isset($tg_message->reply_to['message']['text'])) {
    $post_id = null;
    $action = '';
    // Message to support
    if ($tg_message->reply_to['message']['text'] == 'Техподдержка') {
        $action = 'MessageToSupport';
        // } elseif ($tg_message->$tg_message->reply_to['message']['text'] == '????????????') {
        //
    }
    // Request invoice checking
    if ($tg_message->reply_to['message']['text'] == 'Введите номер счёта') {
        $action = 'getInvoiceInfo';
    }
    // Rechadge balance
    if ($tg_message->reply_to['message']['text'] == 'Введите сумму для пополнения') {
        $action = 'rechargeBalance';
    }
    // Edit Post
    preg_match_all("/^Введите новый текст для сообщения #(\d*)$/", $tg_message->reply_to['message']['text'], $matches);
    if ($matches[1]) {
        $post_id = (int)$matches[1][0];
        if ($post_id > 0) $action = 'editPost';
    }
    dbg("Matches: ", $tg_message->reply_to['message']['text'], "msg_id: $post_id");

    switch ($action) {
        case 'rechargeBalance':
            dbg("Render::rechargeBalance");
            if (array_key_exists('text', $tg_message->message_array)
                && Render::showRechargeBalance($usr, $tg_message, ['2' => $tg_message->message_array['text']])) {
                break;
            } else {
                $tg_message->send([
                    'payload' => ['text' => 'Сумма указана не корректно'],
                ]);
            }
            break;
        case 'getInvoiceInfo':
            dbg("Render::getInvoiceInfo: $post_id");
            Render::getInvoiceInfo($usr, $tg_message, $tg_message->message_array);
            break;
        case 'editPost':        // ok
            dbg("Edit message: $post_id");

            $post = new Post($post_id);
            if (! $post->id) exit();

            // // Edit only 'text' or 'caption'
            // $m_data = json_decode($m[0]->msg, true);
            // if (isset($m_data['text'])) {
            //     $m_data['text'] = $tg_message->text;
            //     $result = $m[0]->edit(json_encode($m_data));
            // } else {
            //     $m_data['caption'] = $tg_message->text;
            //     $result = $m[0]->edit(json_encode($m_data));
            // }

            $result = $post->edit(json_encode($tg_message->message_array));
            dbg("DB edit result: ", $result);

            if ($result) {
                dbg("DB edit result: Success");

                $tg_message->send([
                    'payload' => ['text' => 'Сообщение отредактировано.'],
                    'force_reply' => false,
                ]);

                Render::showPost($usr, $tg_message, $post);

            }
            break;
        // TODO ?
        // case 'MessageToSupport':
        //     // new Telegram($usr->tlg_id, json_encode($tg_message->message_json));
        //     //$MessageToSupport = new Message();
        //
        //     $new_message = [
        //         'payload' => ['text' => 'Сообщение принято. В ближайшее время с вами свяжется сотрудние технической поддержки.'],
        //         'force_reply' => false,
        //     ];
        //     $result = $tg_message->send($new_message);
        //
        //     $new_message = [
        //         'payload' => ['text' => 'Выберите действие: '],
        //         'buttons' => [['Мои рассылки', 'Создать рассылку']],
        //     ];


    }
    $tg_message->send([
        'payload' => ['text' => 'Выберите действие: '],
        'buttons' => Menu::getButtons('main'),
    ]);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Message
    //
} else {
    if (array_key_exists('text', $tg_message->message_array)) {
        $text = $tg_message->message_array['text'];
    } else {
        $text = '~z@';
    }
    dbg("Massage. Text: " . $text);
    switch ($text) {
        case 'Мои рассылки':
            dbg('Render::showPostList');
            Render::showPostList($usr, $tg_message);
            // $tg_message->deleteMessage();
            break;
        case 'Баланс':
            dbg('Render::showUserBalance');
            Render::showUserBalance($usr, $tg_message);
            // $tg_message->deleteMessage();
            break;
        case '/start':
        case (! ! preg_match('/^\/start.*/', $text)):
            $tg_message->send([
                // TODO Добавить описание
                'payload' => ['text' => 'Данный бот предназначен для совершения рассылок в популярные каналы. Напишите в бот сообщение, которое хотите разослать.'],
            ]);
            $tg_message->send([
                'payload' => ['text' => 'Выберите действие: '],
                'buttons' => Menu::getButtons('main'),
            ]);
            $tg_message->deleteMessage();
            break;
        case 'Справка':
            Render::showHelp($usr, $tg_message);
            break;
        case 'Рефералы':
            dbg('Render::showReferrals');
            Render::showReferrals($usr, $tg_message);
            break;
        case 'Заявки':
            dbg('Render::showMailingRequestList');
            Render::showMailingRequestList($usr, $tg_message);
            // $tg_message->deleteMessage();
            break;
        case 'Техподдержка':
            $tg_message->send([
                'payload' => [
                    'text' => "Для связи с технической поддержкой вам следует перейти в чат службы поддержки, нажав кнопку под данным сообщением",
                ],
                'inline_keyboard' => [[
                    ['text' => 'Связаться с поддержкой', 'url' => SUPPORT_URL],
                ]]]);
            // $tg_message->deleteMessage();
            break;
        default: // Draft           //ok
            $tg_message->send([
                'payload' => ['text' => "Добавить в рассылки?"],
                'reply_to_message_id' => $tg_message->message_id,
                'force_reply' => false,
                'inline_keyboard' => [[  // Array of Array of InlineKeyboardButton
                    ['text' => 'Удалить',
                        'callback_data' => '#deletedraft#'],
                    ['text' => 'Добавить', 'callback_data' => '#savedraft#' . $tg_message->message_id . "#"],
                ]],
            ]);
    }
}

if (false) {

    if (! $new_payload) {
        dbg("Default menu");
        $new_payload = [
            'payload' => ['text' => 'Выберите действие: '],
            'buttons' => Menu::getButtons('main'),
        ];
        $result = $tg_message->send($new_payload);
        dbg("Result", $tg_message->result);
    }
}


function dbg()
{
    if (isset($_GET["test"])) {
        $args = func_get_args();
        foreach ($args as $arg) {
            print_r($arg);
            print("\n==============================================\n");
        }
    }
}
