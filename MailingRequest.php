<?php


namespace PostmanBot;


defined('_EXEC') or exit();

class MailingRequest extends BaseModel
{
    public $id = null;
    public $user_id = null;  // queue_pk
    public $post_id = null;  // queue_pk
    public $mailing_group_key = null;  // queue_pk
    public $mailing_group_name = null;
    public $chat_id = null; // for notice
    public $message_id = null; // for notice
    public $stored_message_id = 0;
    public $invoice_id = null;
    // 0 - ?, 1 - draft, 2 - paid, 3 - stored 4 - queued, 5 - q_noticed,
    // 6 - sent, 7 - committed  8 - noticed and finished,
    // 30 31 32 - error
    public $status = 'draft';
    public $queue_id = 0;
    public $created_timestamp = null;
    public $modified_timestamp = null;

    public $per_page = 3;


    public function __construct()
    {
        parent::__construct();

        $args = func_get_args();

        switch (func_num_args()) {
            case 1: // Get item
                if (is_array($args[0])) {
                    foreach ($args[0] as $key => $val) {
                        $this->$key = $val;
                    }
                } else {
                    // TODO TEST
                    $sql = 'SELECT * FROM `' . $this->tablePrefix('mailing_requests') . '` WHERE `' . $this->tablePrefix('mailing_requests') . '`.`id` = :id LIMIT 1';

                    $sth = $this->db->prepare($sql);
                    $sth->bindValue(':id', (int)$args[0], \PDO::PARAM_INT);
                    if ($sth->execute()) {
                        while ($mailing_request = $sth->fetch(\PDO::FETCH_ASSOC)) {
                            $this->id = $mailing_request['id'];
                            $this->user_id = $mailing_request['user_id'];
                            $this->post_id = $mailing_request['post_id'];
                            $this->mailing_group_key = $mailing_request['mailing_group_key'];
                            $this->mailing_group_name = $mailing_request['mailing_group_name'];
                            $this->chat_id = $mailing_request['chat_id'];
                            $this->message_id = $mailing_request['message_id'];
                            $this->stored_message_id = $mailing_request['stored_message_id'];
                            $this->invoice_id = $mailing_request['invoice_id'];
                            $this->status = $mailing_request['status'];
                            $this->queue_id = $mailing_request['queue_id'];
                            $this->created_timestamp = $mailing_request['created_timestamp'];
                            $this->modified_timestamp = $mailing_request['modified_timestamp'];
                        }
                    }
                }
                break;
        }
    }


    public function setStatusAndSave($status)
    {
        $this->status = $status;
        $this->save();
    }


    public function setQueueIdAndSave($queue_id)
    {
        $this->queue_id = $queue_id;
        $this->save();
    }


    public function setStoredAndSave($stored_message_id)
    {
        $this->stored_message_id = $stored_message_id;
        $this->save();
    }


    public function save()
    {
        if ($this->id) {
            $params = [
                'id' => $this->id,
                'status' => $this->status,
                'queue_id' => $this->queue_id,
                'stored_message_id' => $this->stored_message_id,
            ];

            $sql = 'UPDATE `' . $this->tablePrefix('mailing_requests') . '`
                        SET `status` = :status,
                            `queue_id` = :queue_id,
                            `stored_message_id` = :stored_message_id
                        where `' . $this->tablePrefix('mailing_requests') . '`.`id` = :id';
        } else {
            $params = [
                'user_id' => $this->user_id,
                'post_id' => $this->post_id,
                'mailing_group_key' => $this->mailing_group_key,
                'mailing_group_name' => $this->mailing_group_name,
                'chat_id' => $this->chat_id,
                'message_id' => $this->message_id,
                'stored_message_id' => $this->stored_message_id,
                'invoice_id' => $this->invoice_id,
                'status' => $this->status,
                'queue_id' => $this->queue_id,
            ];
            $sql = 'INSERT INTO `' . $this->tablePrefix('mailing_requests') . '` (`user_id`, `post_id`, `mailing_group_key`, `mailing_group_name`, `chat_id`, `message_id`, `stored_message_id`, `invoice_id`, `status`, `queue_id`)
                        VALUES(:user_id, :post_id, :mailing_group_key, :mailing_group_name, :chat_id, :message_id, :stored_message_id, :invoice_id, :status, :queue_id)';
        }
        $sth = $this->db->prepare($sql);
        if ($sth->execute($params)) {
            if (! $this->id)
                $this->id = $this->db->lastInsertId();
            return $this->id;
        }
        return null;
    }


    public function getByInvoiceId($invoice_id)
    {
        $sql = 'SELECT * FROM `' . $this->tablePrefix('mailing_requests') . '` WHERE `' . $this->tablePrefix('mailing_requests') . '`.`invoice_id` = :invoice_id LIMIT 1';

        $sth = $this->db->prepare($sql);
        $sth->bindValue(':invoice_id', (int)$invoice_id, \PDO::PARAM_INT);
        if ($sth->execute()) {
            while ($mailing_request = $sth->fetch(\PDO::FETCH_ASSOC)) {
                $this->id = $mailing_request['id'];
                $this->user_id = $mailing_request['user_id'];
                $this->post_id = $mailing_request['post_id'];
                $this->mailing_group_key = $mailing_request['mailing_group_key'];
                $this->mailing_group_name = $mailing_request['mailing_group_name'];
                $this->chat_id = $mailing_request['chat_id'];
                $this->message_id = $mailing_request['message_id'];
                $this->stored_message_id = $mailing_request['stored_message_id'];
                $this->invoice_id = $mailing_request['invoice_id'];
                $this->status = $mailing_request['status'];
                $this->queue_id = $mailing_request['queue_id'];
                $this->created_timestamp = $mailing_request['created_timestamp'];
                $this->modified_timestamp = $mailing_request['modified_timestamp'];
            }
        }

    }

    public function getPending()
    {
        $sql = "SELECT * FROM `" . $this->tablePrefix('mailing_requests') . "` WHERE
                             (`" . $this->tablePrefix('mailing_requests') . "`.`status` = 1 AND `modified_timestamp` > now() - 24*60*60)
                             OR
                             (`" . $this->tablePrefix('mailing_requests') . "`.`status` not in ('0', 'finished', 'unknown', 'error'))
                             ";

        $mailing_request_array = [];

        $sth = $this->db->prepare($sql);
        if ($sth->execute()) {
            while ($mailing_request = $sth->fetch(\PDO::FETCH_ASSOC)) {
                $this->id = $mailing_request['id'];
                $this->user_id = $mailing_request['user_id'];
                $this->post_id = $mailing_request['post_id'];
                $this->mailing_group_key = $mailing_request['mailing_group_key'];
                $this->mailing_group_name = $mailing_request['mailing_group_name'];
                $this->chat_id = $mailing_request['chat_id'];
                $this->message_id = $mailing_request['message_id'];
                $this->stored_message_id = $mailing_request['stored_message_id'];
                $this->invoice_id = $mailing_request['invoice_id'];
                $this->status = $mailing_request['status'];
                $this->queue_id = $mailing_request['queue_id'];
                $this->created_timestamp = $mailing_request['created_timestamp'];
                $this->modified_timestamp = $mailing_request['modified_timestamp'];

                $mailing_request_array[] = $mailing_request;
            }
        }

        return $mailing_request_array;
    }

}
