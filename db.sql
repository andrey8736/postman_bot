CREATE USER `postmanbot`@`localhost` IDENTIFIED BY 'postmanbot';
GRANT ALL PRIVILEGES ON `postmanbot`.* TO `postmanbot`@`localhost` WITH GRANT OPTION;
CREATE DATABASE `postmanbot` CHARACTER SET utf8 COLLATE utf8_general_ci;


CREATE TABLE `postmanbot`.`users`
(
    `id`        BIGINT       NOT NULL AUTO_INCREMENT,
    `tlg_id`    BIGINT       NOT NULL,
    `user_data` VARCHAR(300) NOT NULL,
    `refer_id`  BIGINT       NOT NULL,
    `indt`      TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE `tlg_id` (`tlg_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `postmanbot`.`msg_list`
(
    `id`      bigint(20)     NOT NULL AUTO_INCREMENT,
    `user_id` bigint(20)     NOT NULL,
    `msg`     varchar(10000) NOT NULL,
    `status`  tinyint(4)     NOT NULL DEFAULT 0,
    `ctime`   timestamp      NOT NULL DEFAULT current_timestamp(),
    `stime`   timestamp      NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`id`),
    KEY `user_id` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `postmanbot`.`mailing_requests`
(
    `id`                 BIGINT(20)                              NOT NULL AUTO_INCREMENT,
    `user_id`            BIGINT(20)                              NOT NULL,
    `post_id`            BIGINT(20)                              NOT NULL,
    `mailing_group_key`  VARCHAR(50)                             NOT NULL,
    `mailing_group_name` VARCHAR(50)                             NOT NULL,
    `invoice_id`         BIGINT(20)                              NOT NULL,
    `chat_id`            BIGINT(20)                              NOT NULL,
    `message_id`         BIGINT(20)                              NOT NULL,
    `stored_message_id`  BIGINT(20)                              NOT NULL DEFAULT 0,
    `status`             VARCHAR(50)                             NOT NULL DEFAULT 'unknown',
    `queue_id`           BIGINT(20)                              NOT NULL DEFAULT 0,
    `created_timestamp`  TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    `modified_timestamp` TIMESTAMP on update CURRENT_TIMESTAMP() NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`id`),
    UNIQUE KEY `invoice_id` (`invoice_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


# CREATE TABLE postmanbot.`invoices`
# (
#     `id`          BIGINT       NOT NULL AUTO_INCREMENT,
#     `invoice_id`  BIGINT       NOT NULL,
#     `user_id`     BIGINT       NOT NULL,
#     `chat_id`     BIGINT       NOT NULL,
#     `message_id`  BIGINT       NOT NULL,
#     `status`      TINYINT      NOT NULL,
#     `post_id`     BIGINT       NOT NULL,
#     `description` VARCHAR(100) NOT NULL,
#     `ctime`       TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
#     `payed_time`  TIMESTAMP    NOT NULL,
#     PRIMARY KEY (`id`),
#     UNIQUE (`invoice_id`)
# ) ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;


