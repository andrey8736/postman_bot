<?php


namespace PostmanBot;


defined('_EXEC') or exit();

use Postmanbot\DataBase;

class Post extends BaseModel
{
    public $id = null;
    public $user_id = null;
    public $msg_json = null;
    public $status = 0;
    public $ctime = null;
    public $stime = null;  // время постановки в очередь таймстемп


    public function __construct()
    {
        parent::__construct();

        $args = func_get_args();
        switch (func_num_args()) {
            case 1: // Get message
                $sql = 'SELECT * FROM `' . $this->tablePrefix('msg_list') . '` WHERE `' . $this->tablePrefix('msg_list') . '`.`id` = :msg_id LIMIT 1';

                $sth = $this->db->prepare($sql);
                $sth->bindValue(':msg_id', (int)$args[0], \PDO::PARAM_INT);
                if ($sth->execute()) {
                    while ($msg = $sth->fetch(\PDO::FETCH_ASSOC)) {
                        $this->id = $msg['id'];
                        $this->user_id = $msg['user_id'];
                        $this->msg_json = $msg['msg'];
                        $this->status = $msg['status'];
                        $this->ctime = $msg['ctime'];
                        $this->stime = $msg['stime'];
                    }
                }
                break;

            case 2: // Save draft
                $this->user_id = $args[0];
                $this->msg_json = $args[1];
                $this->save();
                break;

            case 6: // User->getMessages
                $this->id = $args[0];
                $this->user_id = $args[1];
                $this->msg_json = $args[2];
                $this->status = $args[3];
                $this->ctime = $args[4];
                $this->stime = $args[5];
                break;

            default:
                return;
        }
    }


    public function save()
    {
        if (! ($this->user_id && $this->msg_json)) return false;

        $params = [
            'msg' => $this->msg_json,
            'status' => $this->status,
            //'stime' => $this->ctime,
        ];

        if ($this->id) {
            $params['id'] = $this->id;

            $sql = 'UPDATE `' . $this->tablePrefix('msg_list') . '`
                        SET `msg` = :msg,
                            `status` = :status
                        where `' . $this->tablePrefix('msg_list') . '`.`id` = :id';

        } else {
            $params['user_id'] = $this->user_id;
            $sql = 'INSERT INTO `' . $this->tablePrefix('msg_list') . '` (`user_id`, `msg`, `status`) VALUES(:user_id, :msg, :status)';
        }
        $sth = $this->db->prepare($sql);
        if ($sth->execute($params)) {
            if (! $this->id)
                $this->id = $this->db->lastInsertId();
            return $this->id;
        }
        return null;
    }


    public function delete()
    {
        if (! $this->id) return false;

        $sql = 'DELETE FROM `' . $this->tablePrefix('msg_list') . '` WHERE `id` = :msg_id';

        $sth = $this->db->prepare($sql);
        $sth->bindValue(':msg_id', (int)$this->id, \PDO::PARAM_INT);

        return $sth->execute();
    }


    public function edit($new_msg_json)
    {
        if (! $this->id) return false;

        $sql = 'UPDATE `' . $this->tablePrefix('msg_list') . '`
                        SET `msg` = :new_msg_json
                        where `' . $this->tablePrefix('msg_list') . '`.`id` = :id';

        $sth = $this->db->prepare($sql);
        $sth->bindValue(':id', (int)$this->id, \PDO::PARAM_INT);
        $sth->bindValue(':new_msg_json', $new_msg_json, \PDO::PARAM_STR);

        if ($sth->execute()) {
            $this->msg_json = $new_msg_json;
            return true;
        }
        return false;
    }
}