<?php


namespace PostmanBot;


defined('_EXEC') or exit();

class Menu
{
    public static $main =
        ['Мои рассылки', 'Баланс', 'Справка'];
    //'InlineKeyboardButton'
    public static $footer =
        [['text' => 'Рефералы'], ['text' => 'Заявки'], ['text' => 'Техподдержка']];
    public static $navi =
        ['Вначало', 'Страница N', 'Страница N+1'];


    public static function getButtons($menu, $params = [])
    {
        $buttons = [];
        switch ($menu) {
            case 'main':
                $buttons[] = self::$main;
                $buttons[] = self::$footer;
                break;
            case 'postMenu':
                if (array_key_exists('post_id', $params)) {
                    if (isset($params['tg_msg_id']))
                        $tg_msg_id = $params['tg_msg_id'];
                    else
                        $tg_msg_id = '';
                    $buttons = [
                        [
                            ['text' => 'Удалить', 'callback_data' => '#deletePost#' . $params['post_id'] . "#" . $tg_msg_id],
                            ['text' => 'Редактировать', 'callback_data' => '#editPost#' . $params['post_id'] . "#"],
                        ],
                        [
                            ['text' => 'Разослать', 'callback_data' => '#setPostMenuPosting#' . $params['post_id'] . "#"],
                        ]
                    ];
                }
                break;
            case 'balanceMenu':
                if (array_key_exists('amount', $params))
                    $amount = $params['amount'];
                else
                    $amount = '';
                $buttons = [
                    [
                        ['text' => 'История', 'callback_data' => '#showBalanceHistory#'],
                    ],
                    [
                        ['text' => 'Пополнить', 'callback_data' => '#setUserBalanceMenuPayment#'],
                    ],
                ];

                break;

        }
        return $buttons;
    }


    public static function getMarkup($menu, $params = [], array &$payload = [])
    {
        $buttons = [];
        switch ($menu) {
            case 'main':
                $buttons = self::getButtons('main');
                break;
            case 'postMenu':
                $buttons = self::getButtons('postMenu', $params);
                break;
        }
        $payload['reply_markup'] = [
            'inline_keyboard' => $buttons,
        ];
        return $payload['reply_markup'];
    }


    public function action($action, $params = [])
    {
        return [];
    }
}


