<?php


namespace PostmanBot;


defined('_EXEC') or exit();

class Message extends Telegram
{
    // Chat
    public $chat_id = null;  //cb   msg
    // public $chat_title = null;
    // public $chat_type = null;
    // public $chat_all_members_are_administrators = null;

    // Message
    public $message_id = null;  // cb   msg
    public $message_array = null;  // cb msg

    // From
    public $from_array = null; // cb msg
    public $user_tg_id = null;  // cb   msg
    public $from_is_bot = null;  // cb  msg
    // public $from_first_name = null;
    // public $from_username = null;

    public $callback_data = null;  // cb
    public $callback_id = null;  // cb
    public $chat_instance = null;  //cb

    public $reply_to = [];  // cb {message_id, message}

    // private $keyboard = [
    //     'resize_keyboard' => true,
    //     'one_time_keyboard' => false,
    //     'selective' => true,
    // ];


    public function __construct($token, $json_data)
    {
        $data = json_decode($json_data, true);

        $this->token = $token;
        if (isset($data['callback_query'])) {
            if (isset($data['callback_query']['data'])) {
                $this->callback_data = $data['callback_query']['data'];
            }
            $this->callback_id = $data['callback_query']['id'];
            $this->chat_instance = $data['callback_query']['chat_instance'];
            $this->chat_id = $data['callback_query']['message']['chat']['id'];
            $this->message_id = $data['callback_query']['message']['message_id'];
            $this->message_array = $data['callback_query']['message'];
            $this->user_tg_id = $data['callback_query']['from']['id'];
            $this->from_array = $data['callback_query']['from'];
            $this->from_is_bot = $data['callback_query']['from']['is_bot'];

            if (isset($data['callback_query']['message']['reply_to_message'])) {
                $this->reply_to['message_id'] = $data['callback_query']['message']['reply_to_message']['message_id'];
                $this->reply_to['message'] = $data['callback_query']['message']['reply_to_message'];
            }

        } elseif (isset($data['message'])) {
            if (isset($data['message']['chat']['id'])) {
                $this->message_array = $data['message'];
                $this->chat_id = $data['message']['chat']['id'];
                $this->message_id = $data['message']['message_id'];
                $this->user_tg_id = $data['message']['from']['id'];
                $this->from_array = $data['message']['from'];
                $this->from_is_bot = $data['message']['from']['is_bot'];

                if (isset($data['message']['reply_to_message'])) {
                    $this->reply_to['message'] = $data['message']['reply_to_message'];
                    $this->reply_to['message_id'] = $data['message']['reply_to_message']['message_id'];
                }
            }
        }
    }


    public function deleteMessage($message_id_or_payload = null, $chat_id = null)
    {
        if (is_array($message_id_or_payload)) {
            $payload = $message_id_or_payload;
        } else {
            $payload['chat_id'] = $chat_id ? $chat_id : $this->chat_id;
            $payload['message_id'] = $message_id_or_payload ? $message_id_or_payload : $this->message_id;
        }
        return parent::deleteMessage($payload);
    }


    public function editMessageText($text_or_payload = null)
    {
        if (is_array($text_or_payload)) {
            $payload = $text_or_payload;
        } else {
            $payload['text'] = $text_or_payload;
        }
        $payload['chat_id'] = $this->chat_id;
        $payload['message_id'] = $this->message_id;
        return parent::editMessageText($payload);
    }


    public function editMessage($payload = null)
    {
        $payload['chat_id'] = $this->chat_id;
        $payload['message_id'] = $this->message_id;
        return parent::editMessageText($payload);
    }


    public function editMessageReplyMarkup($payload)
    {
        $payload['chat_id'] = $this->chat_id;
        $payload['message_id'] = $this->message_id;
        return parent::editMessageReplyMarkup($payload);
    }


    public function send(array $params)
    {
        $params['chat_id'] = $this->chat_id;
        $params['message_id'] = $this->message_id;
        return parent::send($params);
    }
}