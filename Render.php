<?php


namespace PostmanBot;

use PostmanBot\Telegram;
use PostmanBot\User;
use PostmanBot\Message;

defined('_EXEC') or exit();

class Render
{

    static private $invoice_status_map = [
        'draft' => 'Черновик',
        'not_paid' => 'Не оплачен',
        'partially_paid' => 'Оплачен частично',
        'fully_paid' => 'Оплачен',
        'overdue' => 'Просрочен',
        'cancelled' => 'Отменен',
    ];

    // static private $mailing_request_status_map = [
    //     0 => 'N/A',
    //     1 => 'Черновик',
    //     2 => 'Оплачено',
    //     3 => 'В очереди',
    //     4 => 'В очереди',
    //     5 => 'Разослано',
    //     6 => 'Завершено',
    //     7 => 'Завершено',
    //     30 => 'Ошибка',
    //     31 => 'Ошибка',
    //     32 => 'Ошибка',
    //     33 => 'Ошибка. Средства возвращены',
    // ];

    static private $mailing_request_status_map = [
        0 => 'Неопределен',
        'unknown' => 'неопределен',

        'draft' => 'черновик',
        'paid' => 'оплачено',
        'stored' => 'в очереди',
        'queued' => 'в очереди',
        'q_noticed' => 'а очереди',
        'sent' => 'разослано',
        'committed' => 'разослано',
        'finished' => 'завершено',

        'error' => 'ошибка',
        'error_noticed' => 'ошибка',
        'error_refund' => 'ошибка',
        'error_refund_noticed' => 'ошибка. Средства возвращены',
    ];


    // Shortens a number and attaches K, M, B, etc. accordingly
    static private function number_shorten($number, $precision = 3, $divisors = null)
    {

        // Setup default $divisors if not provided
        if (!isset($divisors)) {
            $divisors = array(
                pow(1000, 0) => '', // 1000^0 == 1
                pow(1000, 1) => 'K', // Thousand
                pow(1000, 2) => 'M', // Million
                pow(1000, 3) => 'B', // Billion
                pow(1000, 4) => 'T', // Trillion
                pow(1000, 5) => 'Qa', // Quadrillion
                pow(1000, 6) => 'Qi', // Quintillion
            );
        }

        // Loop through each $divisor and find the
        // lowest amount that matches
        foreach ($divisors as $divisor => $shorthand) {
            if (abs($number) < ($divisor * 1000)) {
                // We found a match!
                break;
            }
        }

        // We found our match, or there were no matches.
        // Either way, use the last defined value for $divisor.
        return number_format($number / $divisor, $precision) . $shorthand;
    }


    static public function showPost(User &$user, Message &$tg_message, &$post, $tg_msg_id = '')
    {
        // Header
        // if ($show_header) {
        //     $new_message = [
        //         'payload' => ['text' => $post->getHeader()],
        //         'force_reply' => false,
        //     ];
        //     $result = $tg_message->send($new_message);
        //     if ($result['ok']) {
        //         $tg_msg_id .= "," . $result['result']['message_id'];
        //     }
        // }

        // Message
        $new_message['payload'] = [];
        $new_message['method'] = $tg_message->getMedia(json_decode($post->msg_json, true), $new_message['payload']);
        // $new_message['inline_keyboard'] = Menu::getButtons('postMenu', ['post_id' => $post->id, 'tg_msg_id' =>$tg_msg_id] );
        Menu::getMarkup('postMenu', ['post_id' => $post->id, 'tg_msg_id' => $tg_msg_id], $new_message['payload']);
        $tg_message->send($new_message);
    }


    static public function showPostList(User &$user, Message &$tg_message, array $callback_data = [])
    {
        $page = 1;
        if (array_key_exists(2, $callback_data)) {
            if ($callback_data[2])
                $page = $callback_data[2];
        }

        $all_posts_statuses = $user->getAllPostStatuses();
        $all_tg_msg_ids = '';
        foreach ($user->getPosts($page) as $post) {
            $tg_msg_id = '';
            // Header
            $post_total = 0;
            $post_statuses = '';  // "\nЗаявки отсутствуют";
            if (array_key_exists($post->id, $all_posts_statuses)) {
                $post_statuses = "\n";
                foreach ($all_posts_statuses[$post->id] as $status => $sub_total) {
                    $post_statuses .= self::$mailing_request_status_map[$status] . ": " . $sub_total . "\n";
                    $post_total += $sub_total;
                }
            }
            $header = '№' . $post->id . ' Добавлено: '
                // . date("d.m.Y H:i:s", strtotime($post->ctime)) . "\n"
                . date("d.m.Y", strtotime($post->ctime)) . "\n"
                . 'Заявки на рассылку: ' . $post_total . $post_statuses . "\n";
            // if ($post->stime != '0000-00-00 00:00:00') {
            //     $header .= "Поставлено в очередь " . $post->stime;
            // } else {
            //     $header .= 'Данное сообщение еще ни разу не рассылалось';
            // }


            $new_message = [
                'payload' => ['text' => $header],
                'force_reply' => false,
            ];
            $result = $tg_message->send($new_message);
            if ($result['ok']) {
                $tg_msg_id = $result['result']['message_id'];
                $all_tg_msg_ids .= "," . $tg_msg_id;
            }

            // Message
            $new_message['payload'] = [];
            Menu::getMarkup('postMenu', ['post_id' => $post->id, 'tg_msg_id' => $tg_msg_id], $new_message['payload']);
            $new_message['method'] = $tg_message->getMedia(json_decode($post->msg_json, true), $new_message['payload']);
            $tg_message->send($new_message);


            // Рекомендуется отправлять не более 1 сообщения в секунду
            //sleep(1);
        }
        $total_messages = $user->getTotalPosts();
        $total_pages = ceil($total_messages / $user->per_page);

        $new_message = [
            'payload' => ['text' => "Всего сообщений: $total_messages \n"
                . "Страница $page из $total_pages"]
        ];

        // Navigation buttons
        $navi_buttons = [];
        if ($page - 1 > 0) {
            $navi_buttons[] = ['text' => '<< Cтр. ' . ($page - 1),
                'callback_data' => '#showPostList#' . ($page - 1) . "#"];
        }

        //$navi_buttons[] = ['text' => 'Очистить', 'callback_data' => '#clearPosts#' . $all_tg_msg_ids . "#"],

        if ($page + 1 <= $total_pages) {
            $navi_buttons[] = ['text' => 'Cтр. ' . ($page + 1) . " >>",
                'callback_data' => '#showPostList#' . ($page + 1) . "#"];
        }
        $new_message['inline_keyboard'] = [$navi_buttons];
        $tg_message->send($new_message);
    }


    static public function setPostMenu(User &$user, Message &$tg_message, array $callback_data)
    {
        $post_id = null;
        if (array_key_exists(2, $callback_data)) {
            $post_id = $callback_data[2];
        }
        if (! $post_id)
            return;

        $new_payload = [];
        Menu::getMarkup('postMenu', ['post_id' => $post_id], $new_payload);

        $tg_message->editMessageReplyMarkup($new_payload);
    }


    static public function setPostMenuPosting(User &$user, Message &$tg_message, array $callback_data)
    {
        $post_id = null;
        if (array_key_exists(2, $callback_data)) {
            $post_id = $callback_data[2];
        }
        if (! $post_id)
            return;

        $page = 1;
        if (array_key_exists(3, $callback_data))
            if ($callback_data[3])
                $page = $callback_data[3];

        $groups = IngdgApi::getGroups();
        if (! $groups)
            return;
        $groups_count = count($groups);

        $per_page = 7;
        $prev_page = $page - 1;
        $next_page = $page + 1;
        $last_page = ceil($groups_count / $per_page);
        if ($prev_page < 1)
            $prev_page = 1;
        if ($next_page > $last_page)
            $next_page = $last_page;

        $inline_keyboard = [];
        $i = 0;
        foreach ($groups as $group_key => $group) {
            $i += 1;
            if (ceil($i / $per_page) < $page)
                continue;
            if ($i / $per_page > $page)
                break;

            $subscribers = self::number_shorten($group['subscribers'], 1); //, 0, ',', ' ');
            // $subscribers = number_format($group['subscribers'], 0, ',', ' ');
            $price = number_format($group['price'], 0, ',', ' ');
            $inline_keyboard[] = [[
                'text' => $group['name_group']
                    . ' 🧍‍♂️' . $subscribers
                    . '  💰' . $price . 'р.',
                'callback_data' => '#setPostMenuPayment#' . $post_id . '#' . $group_key . '#' . $group['price'],
            ]];
        }
        $inline_keyboard[] = [['text' => 'Заявки',
            'callback_data' => '#showMailingRequestList#' . $post_id . '#0#' . $tg_message->message_id . '#']];

        $inline_keyboard[] = [
            ['text' => '<<', 'callback_data' => '#setPostMenuPosting#' . $post_id . '#' . $prev_page],
            ['text' => 'Отмена', 'callback_data' => '#setPostMenu#' . $post_id],
            ['text' => '>>', 'callback_data' => '#setPostMenuPosting#' . $post_id . '#' . $next_page],
        ];

        $new_payload = [
            'reply_markup' => ['inline_keyboard' => $inline_keyboard],
        ];
        $tg_message->editMessageReplyMarkup($new_payload);
    }


    static public function setPostMenuPayment(User &$user, Message &$tg_message, array $callback_data)
    {
        $post_id = null;
        $group_key = null;
        $price = null;
        if (array_key_exists(2, $callback_data)) {
            $post_id = $callback_data[2];
        }
        if (array_key_exists(3, $callback_data)) {
            $group_key = $callback_data[3];
        }
        if (array_key_exists(4, $callback_data)) {
            $price = $callback_data[4];
        }
        if (! ($post_id && $group_key && $price))
            return;

        $balance_total = $user->getBalance()['total'];
        $balance_hold = $user->getBalance()['hold'];
        $balance_available = $balance_total; // - $balance_hold;
        // $discount = $user->getDiscount();

        if (in_array($user->tlg_id, WITHOUT_PAY, false)) {
            $inline_keyboard[] = [[
                'text' => 'Заказать рассылку',
                'callback_data' => '#doPayment#' . $post_id . '#' . $group_key . '#' . 0,
            ]];
        } elseif ($balance_available < $price) {
            $amount = ceil($price - $balance_available);
            $payment = [
                'tlg_id' => $user->tlg_id,
                'amount' => $amount,
                'currency' => 'RUB',
                'title' => 'Пополнение баланса',
                'description' => 'Пополнение баланса',
                'service' => 'simonline',
                'redirect_result' => 'https://t.me/SimOnlineBot',
            ];

            $invoice = IngdgApi::getInvoice($payment);

            if (! $invoice)
                return;

            $inline_keyboard[] = [[
                'text' => '💰Пополнить баланс на '
                    . number_format($amount, 0, ',', ' ')
                    . ' ' . 'р.',
                'url' => $invoice['link_to_payment'],
            ]];
        } else {

            $price_formatted = number_format($price, 0, ',', ' ');

            $inline_keyboard[] = [[
                'text' => 'Оплатить '
                    . '  💰' . $price_formatted . 'р.',
                'callback_data' => '#doPayment#' . $post_id . '#' . $group_key . '#' . $price,
            ]];
        }


        $inline_keyboard[] = [['text' => 'Отмена',
            'callback_data' => '#setPostMenuPosting#' . $post_id . '#' . 1]];

        $new_payload = [
            'reply_markup' => ['inline_keyboard' => $inline_keyboard],
        ];
        $tg_message->editMessageReplyMarkup($new_payload);
    }


    static public function doPayment(User &$user, Message &$tg_message, array $callback_data)
    {
        try {
            $post_id = $callback_data[2];
            $group_key = $callback_data[3];
            $price = $callback_data[4];
        } catch (\Exception $e) {
            return;
        }

        $post = new Post($post_id);
        if (! $post->id)
            return;

        $groups = IngdgApi::getGroups();
        if (! $groups)
            return;

        if (in_array($user->tlg_id, WITHOUT_PAY, false)) {
            $invoice['invoice_id'] = 0;
        } else {
            $payment = [
                'tlg_id' => $user->tlg_id,
                'amount' => $price,
                'hold' => $price,
                'currency' => 'RUB',
                'title' => 'Рассылка в ' . $groups[$group_key]['name_group'],
                'description' => 'Оплата рассылки сообщения №' . $post_id, // . ' в группу "' . $groups[$group_key]['name_group'] . '"' ,
                'service' => 'simonline',
                'redirect_result' => 'https://t.me/SimOnlineBot',
            ];
            $invoice = IngdgApi::getInvoice($payment);

            if (! $invoice)
                return;
        }

        $inline_keyboard[] = [[
            'text' => 'Оплачено. Вернутся назад',
            'callback_data' => '#setPostMenuPosting#' . $post_id . '#' . 1,
        ]];
        $new_payload = [
            'reply_markup' => ['inline_keyboard' => $inline_keyboard],
        ];

        $mailing_request_data = [
            'user_id' => $user->tlg_id,
            'post_id' => $post_id,
            'mailing_group_key' => $group_key,
            'mailing_group_name' => $groups[$group_key]['name_group'],
            'chat_id' => $tg_message->chat_id,
            'message_id' => $tg_message->message_id,
            'invoice_id' => $invoice['invoice_id'],
            'status' => 'paid',
        ];
        $mailing_request = new MailingRequest($mailing_request_data);
        if ($mailing_request->save()) {
            $tg_message->editMessageReplyMarkup($new_payload);
// TODO Date FORMAT
            $mailing_request_text = 'Заявка №' . $mailing_request->id . ' (' . date('d.m.Y', strtotime($mailing_request->created_timestamp)) . ")\n"
                . '   Пост №' . $mailing_request->post_id . "\n"
                . '   Группа: ' . $mailing_request->mailing_group_name . "\n "
                . '  Статус: ' . self::$mailing_request_status_map[$mailing_request->status]
// TODO Date FORMAT
                . ' (' . date('d.m.Y', strtotime($mailing_request->modified_timestamp)) . ")\n ";
            $tg_message->send([
                'payload' => ['text' => $mailing_request_text],
                'reply_to_message_id' => $tg_message->message_id,
                'force_reply' => false,
            ]);
        }
    }


    static public function showMailingRequestList(User &$user, Message &$tg_message, array $callback_data = [])
    {
        $post_id = 0;
        $page = 1;
        $tg_msg_id = 0;
        if (array_key_exists(2, $callback_data)) {
            if ($callback_data[2])
                $post_id = $callback_data[2];
        }
        if (array_key_exists(3, $callback_data)) {
            if ($callback_data[3])
                $page = $callback_data[3];
        }
        if (array_key_exists(4, $callback_data)) {
            if ($callback_data[4])
                $tg_msg_id = $callback_data[4];
        }

        $post_mailing_requests = $user->getMailingRequests($post_id, $page);
        $total_post_mailing_requests = $user->getTotalMailingRequests($post_id);
        $total_mailing_requests = $user->getTotalMailingRequests(null);
        $total_pages = ceil($total_post_mailing_requests / $user->per_page);

        foreach ($post_mailing_requests as $mailing_request) {
            if ($mailing_request->queue_id)
                $queue_id_text = '' . $mailing_request->queue_id;
            else
                $queue_id_text = 'ожидается...';

            $mailing_request_text = 'Заявка №' . $mailing_request->id . ' (' . date('d.m.Y', strtotime($mailing_request->created_timestamp)) . ")\n"
                // . 'user_id: ' . $mailing_request->user_id . "\n"
                . '   Пост №' . $mailing_request->post_id . "\n"
                // . '$mailing_group_key: ' . $mailing_request->mailing_group_key . "\n"
                . '   Группа: ' . $mailing_request->mailing_group_name . "\n "
                // . 'chat_id: ' . $mailing_request->chat_id . "\n "
                // . 'message_id: ' . $mailing_request->message_id . "\n "
                . '  Счёт №' . $mailing_request->invoice_id . "\n "
                . '  Очередь: ' . $queue_id_text . "\n "
                . '  Статус: ' . self::$mailing_request_status_map[$mailing_request->status]
                . ' (' . date('d.m.Y', strtotime($mailing_request->modified_timestamp)) . ")\n "
                // . 'created_timestamp: ' . $mailing_request->created_timestamp . "\n "
                // . 'modified_timestamp: ' . $mailing_request->modified_timestamp . "\n ";
            ;
            $tg_message->send([
                'payload' => ['text' => $mailing_request_text],
                'reply_to_message_id' => $tg_msg_id,
                'force_reply' => false,
            ]);
        }
        if ($total_mailing_requests) {
            $text = "Общее количество заявок на рассылку: $total_mailing_requests\n";
            if ($post_id)
                $text .= "Заявок на рассылку по данному сообщению: $total_post_mailing_requests\n";
            if ($total_pages > 1)
                $text .= "Страница $page из $total_pages\n";
        } else {
            $text = "Заявки отсутствуют";
        }
        // Navigation buttons
        $navi_buttons = [];
        if ($page - 1 > 0) {
            $navi_buttons[] = ['text' => '<< Cтр. ' . ($page - 1),
                'callback_data' => '#showMailingRequestList#' . $post_id . '#' . ($page - 1) . "#" . $tg_msg_id . '#'];
        }
        if ($page + 1 <= $total_pages) {
            $navi_buttons[] = ['text' => 'Cтр. ' . ($page + 1) . " >>",
                'callback_data' => '#showMailingRequestList#' . $post_id . '#' . ($page + 1) . "#" . $tg_msg_id . '#'];
        }
        $new_payload = [
            'payload' => ['text' => $text],
            'reply_to_message_id' => $tg_msg_id,
            'inline_keyboard' => [$navi_buttons],
        ];

        $new_payload['inline_keyboard'][] = [
            ['text' => 'Связаться с поддержкой', 'url' => SUPPORT_URL],
            ['text' => 'Проверить счёт', 'callback_data' => '#getInvoiceInfoRequest#'],
        ];
        // $new_payload['messageentity'] = [
        //     'type' => 'url',
        //     'switch_inline_query' => 'https://yandex.ru',
        // ];

        $tg_message->send($new_payload);
    }


    static public function showHelp(User &$user, $tg_message, array $callback_data = [])
    {
        $button = null;
        $post_id = null;
        $group_key = null;
        $price = null;
        if (array_key_exists(2, $callback_data)) {
            $button = $callback_data[2];
        }

        // TODO Добавить описание
        $text = 'Данный бот предназначен для совершения рассылок в популярные каналы. Напишите в бот сообщение, которое хотите разослать.';
        switch ($button) {
            case 'Закрыть':
                $tg_message->deleteMessage();
                return;
                break;
            case 'Мои рассылки':
                $text = "\"Мои рассылки\"\nВ данном разделе вы можете посмотреть, удалить и отредактировать все ваши сообщения, предназначенные для рассылки. А также выполнить рассылку в различные каналы.";
                break;
            case 'Баланс':
                $text = "\"Баланс\"\nЗапрос баланса";
                break;
            case 'Рефералы':
                $text = "\"Рефералы\"\nРеферальная программа";
                break;
            case 'Заявки':
                $text = "\"Заявки\"\nЗаявки на рассылку";
                break;
        }
        $keyboard = [
            'inline_keyboard' => [
                [
                    ['text' => 'Мои рассылки', 'callback_data' => '#showHelp#Мои рассылки'],
                    ['text' => 'Баланс', 'callback_data' => '#showHelp#Баланс'],
                ],
                [
                    ['text' => 'Рефералы', 'callback_data' => '#showHelp#Рефералы'],
                    ['text' => 'Заявки', 'callback_data' => '#showHelp#Заявки'],
                ],
                [
                    ['text' => 'Закрыть справку', 'callback_data' => '#showHelp#Закрыть'],
                ],
            ],
        ];
        if (! $button) {
            // $tg_message->deleteMessage();
            $payload = $keyboard;
            $payload['payload'] = ['text' => $text];
            $tg_message->send($payload);
        } else {
            $payload = [
                'text' => $text,
                'reply_markup' => $keyboard,
            ];
            $tg_message->editMessage($payload);
        }
    }


    static public function showUserBalance(User &$user, &$tg_message)
    {
        $balance = $user->getBalance();
        $total = $balance['total'];
        $hold = $balance['hold'];
        $available = $total; // - $hold;
        $available = number_format($available, 0, ',', ' ');

        $text = 'Ваш баланс: ' . $available . ' р.';
        if ($hold)
            $text .= "\nВ заморозке: " . $hold . ' р.';
        $tg_message->send([
            'payload' => ['text' => $text],
            'inline_keyboard' => Menu::getButtons('balanceMenu')
        ]);
    }


    static public function setUserBalanceMenuPayment(User &$user, Message &$tg_message, array $callback_data)
    {
        if (array_key_exists(2, $callback_data) && is_numeric($callback_data[2])) {
            $amount = $callback_data[2];
        } else {
            $amount = 0;
        }

        if ($amount > 0) {
            $payment = [
                'tlg_id' => $user->tlg_id,
                'amount' => $amount,
                'currency' => 'RUB',
                'title' => 'Пополнение баланса',
                'description' => 'Пополнение баланса',
                'service' => 'simonline',
                'redirect_result' => 'https://t.me/SimOnlineBot',
            ];

            $invoice = IngdgApi::getInvoice($payment);
            if ($invoice)
                $invoice_url = $invoice['link_to_payment'];
            else
                $invoice_url = '';

            $inline_keyboard = [
                [
                    ['text' => 'Оплатить ' . $amount . 'р.', 'url' => $invoice_url],
                ],
                [
                    ['text' => 'Отмена', 'callback_data' => '#setUserBalanceMenuPayment#'],
                ],
            ];
        } elseif ($amount == -1) {
            $inline_keyboard = Menu::getButtons('balanceMenu');
        } else {
            $inline_keyboard = [
                [
                    ['text' => '250 р.', 'callback_data' => '#setUserBalanceMenuPayment#250'],
                    ['text' => '500 р.', 'callback_data' => '#setUserBalanceMenuPayment#500'],
                ],
                [
                    ['text' => '1000 р.', 'callback_data' => '#setUserBalanceMenuPayment#1000'],
                    ['text' => '...', 'callback_data' => '#setUserBalanceMenuPayment#-2'],
                ],
                [
                    ['text' => 'Отмена', 'callback_data' => '#setUserBalanceMenuPayment#-1'],
                ],
            ];
        }
        $new_payload = [
            'reply_markup' => ['inline_keyboard' => $inline_keyboard],
        ];
        $tg_message->editMessageReplyMarkup($new_payload);

        if ($amount == -2) {
            $tg_message->send([
                'payload' => ['text' => 'Введите сумму для пополнения'],
                'force_reply' => true,
            ]);
        }
    }


    // static public function rechargeBalanceRequest(User &$user, Message &$tg_message, array $callback_data=[])
    // {
    //     if (array_key_exists(2, $callback_data) && $callback_data=[2]) {
    //         $amount = $callback_data=[2];
    //     } else {
    //         $amount = 0;
    //     }
    //
    //     if ($amount) {
    //         $tg_message->send([
    //             'payload' => ['text' => 'Введите сумму'],
    //             'inline_keyboard' => [
    //                 [
    //                     ['text' => '250р.', 'callback_data' => '#rechargeBalanceRequest#250'],
    //                 ],
    //                 [
    //                     ['text' => 'Пополнить', 'callback_data' => '#rechargeBalanceRequest#'],
    //                 ],
    //             ],
    //         ]);
    //     } else {
    //         $tg_message->send([
    //             'payload' => ['text' => 'Введите сумму для пополнения'],
    //             'force_reply' => true,
    //         ]);
    //     }
    // }
    //
    //
    static public function showRechargeBalance(User &$user, Message &$tg_message, array $callback_data = [])
    {
        if (array_key_exists('2', $callback_data) && is_numeric($callback_data[2]) && $callback_data[2] > 0) {
            $amount = ceil($callback_data[2]);
            $payment = [
                'tlg_id' => $user->tlg_id,
                'amount' => $amount,
                'currency' => 'RUB',
                'title' => 'Пополнение баланса',
                'description' => 'Пополнение баланса',
                'service' => 'simonline',
                'redirect_result' => 'https://t.me/SimOnlineBot',
            ];

            $invoice = IngdgApi::getInvoice($payment);

            if ($invoice)
                return $tg_message->send([
                    'payload' => ['text' => 'Счёт №'
                        . $invoice['invoice_id'] . "\n"
                        . $payment['title'] . "\n"
                        . "Сумма: " . $amount],
                    'inline_keyboard' => [[['text' => 'Оплатить', 'url' => $invoice['link_to_payment']]]]
                ]);
        }
        return false;
    }


    static public function showBalanceHistory(User &$user, Message &$tg_message, array $callback_data = [])
    {
        $invoices = IngdgApi::getUserBalanceHistory($user->tlg_id, 'fully_paid');  //'draft');
        if ($invoices) {

            $tg_message->send([
                'payload' => ['text' => '5 последних пополнений баланса'],
            ]);
            $i = 0;
            foreach ($invoices as $invoice) {
                $tg_message->send([
                    'payload' => ['text' =>
                        'Счёт №' . $invoices[$i]['id']
                        . ' (' . $invoices[$i]['bill_date'] . ")\n"
                        . 'Статус: ' . self::$invoice_status_map[$invoices[$i]['status']] . "\n"
                        . 'Сумма: ' . $invoices[$i]['invoice_value'] . ' р.'
                        // . '' . $invoices[$i]['payment_received'];
                    ]
                ]);
                $i += 1;
                // Limit
                if ($i >= 5)
                    break;
            }
        } else {
            $tg_message->send([
                'payload' => ['text' => 'Пополнений баланса не производилось']
            ]);
        }

    }


    static public function deleteDraft(User &$user, &$tg_message)
    {
        $tg_message->deleteMessage($tg_message->reply_to['message_id']);
        $tg_message->deleteMessage();
    }


    static public function saveDraft(User &$user, &$tg_message)
    {
        $post = new Post($user->tlg_id, json_encode($tg_message->reply_to['message']));
        if ($post->id) {
            $tg_message->editMessageText('Добавлено');
            self::showPost($user, $tg_message, $post,
                $tg_message->message_id . "," . $tg_message->reply_to['message_id']);
        }
    }


    static public function deletePost(User &$user, Message &$tg_message, array $callback_data)
    {
        $post_id = null;
        if (array_key_exists(2, $callback_data)) {
            $post_id = $callback_data[2];
        }
        if (! $post_id)
            return;

        // Telegram messages chain
        $tg_msg_id_array = [];
        if ($callback_data[3])
            $tg_msg_id_array = explode(',', $callback_data[3]);

        $post = new Post($post_id);
        if ($post->delete()) {
            $tg_message->deleteMessage();
            // Delete messages chain
            foreach ($tg_msg_id_array as $tg_msg_id) {
                if ($tg_msg_id)
                    $tg_message->deleteMessage($tg_msg_id);
            }
        }
    }


    static public function editPost(User &$user, Message &$tg_message, array $callback_data)
    {
        $post_id = null;
        if (array_key_exists(2, $callback_data)) {
            $post_id = $callback_data[2];
        }
        if (! $post_id)
            return;
        $tg_message->send([
            'payload' => ['text' => 'Введите новый текст для сообщения #' . $post_id],
            'force_reply' => true,
        ]);
    }


    static public function showReferrals(User &$user, &$tg_message)
    {
        $referral_url = IngdgApi::getUserReferralUrl($user->tlg_id);
        $referral_info = IngdgApi::getUserReferral($user->tlg_id);
        $tg_message->send([
            'payload' => [
                'text' => 'Всего рефералов: ' . number_format((int)$referral_info['count'], 0, ',', ' ')
                    . "\nНачислений за рефералов: " . number_format((int)$referral_info['sum'], 0, ',', ' ') . ' р.',
            ],
            'inline_keyboard' => [
                [['text' => 'Поделиться ссылкой', 'switch_inline_query' => $referral_url]],
                // [['text' => 'Ссылка', 'url' => $referral_url]]
            ],
        ]);
        // switch_inline_query

        $tg_message->send([
            'payload' => [
                'text' => $referral_url,
                //                    'url' => $referral_url,
                //                    'entities' => [['type' => 'url', 'url' => $referral_url]]
            ],
            'force_reply' => false,
        ]);
        // $tg_message->send([
        //     'payload' => [
        //         'text' => $referral_url,
        //         'url' => $referral_url,
        //         'entities' => [['type' => 'url', 'url' => $referral_url]]
        //     ],
        //     'force_reply' => false,
        // ]);
        // switch_inline_query

        // $tg_message->deleteMessage();
    }


    static public function getInvoiceInfoRequest(User &$user, Message &$tg_message, array $callback_data = null)
    {
        $tg_message->send([
            'payload' => ['text' => 'Введите номер счёта'],
            'force_reply' => true,
        ]);
    }


    static private function showInvoice(User &$user, Message &$tg_message, $invoice_id)
    {

        $invoice_info = IngdgApi::getInvoiceInfo($invoice_id);
        if (array_key_exists('ok', $invoice_info) && $invoice_info['ok']) {
            if (array_key_exists('tlg_id', $invoice_info) && $invoice_info['tlg_id'] == $user->tlg_id) {
                $invoice_text =
                    '№' . $invoice_id . "\n"
                    . $invoice_info['invoice_items'][0]['title'] . "\n"
                    . $invoice_info['invoice_items'][0]['description'] . "\n"
                    . 'Сумма' . $invoice_info['invoice_info']['invoice_value'] . "\n"
                    . 'Статус: ' . self::$invoice_status_map[$invoice_info['invoice_info']['status']] . "\n";

                if ($tg_message->send(['payload' => ['text' => $invoice_text]]))
                    return true;
            }
        }

        return false;
    }


    static public function getInvoiceInfo(User &$user, Message &$tg_message, array $callback_data)
    {
        if (array_key_exists('text', $callback_data) && is_numeric($callback_data['text']) && $callback_data['text'] > 0) {
            if (self::showInvoice($user, $tg_message, $callback_data['text']))
                return;
        }
        // Incorrect request
        $tg_message->send([
            'payload' => ['text' => "Введен некорректный номер счета, либо счет не закреплен за вами.\nВы всегда можете воспользоваться услугами нашей технической поддержки."],
            'inline_keyboard' => [[
                ['text' => 'Связаться с поддержкой', 'url' => SUPPORT_URL],
                ['text' => 'Проверить счёт', 'callback_data' => '#getInvoiceInfoRequest#'],
            ]]
        ]);
    }

}
