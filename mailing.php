<?php

define('_EXEC', 'mailing');

include('config.php');

use PostmanBot\IngdgApi;
use PostmanBot\MailingRequest;
use PostmanBot\Telegram;
use PostmanBot\Message;
use PostmanBot\Post;


if (isset($_GET['TOKEN'])) {
    if ($_GET['TOKEN'] != TOKEN) {
        http_response_code(404);
        exit();
    }
} else {
    http_response_code(404);
    exit();
}
if (isset($_GET['test'])) {
    $data = file_get_contents('mailing_request.json');
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
} else {
    $data = file_get_contents('php://input');
    file_put_contents('mailing_request.json', $data);
    // if ($data)
    //     file_put_contents('./payments/' . date('Y-m-d_H-i-s.', time()) . 'payment_request.txt', $data);
}

//todo del
//if (! isset($_GET['request_id'])) exit();
//file_put_contents('request.json', json_encode($_REQUEST) . "\n\n",FILE_APPEND );
//file_put_contents('server.json', json_encode($_SERVER['REQUEST_URI']) . "\n\n",FILE_APPEND );

if (is_array($data))
    $data_array = json_decode($data, true);
else
    $data_array = [];

dbg("data_array:", $data_array);

if (isset($_GET['request_id']) && is_numeric($_GET['request_id']) && $_GET['request_id']) {
    dbg('GET: request_id: ' . $_GET['request_id']);
    file_put_contents('mr_id.json', $_GET['request_id'] . "\n", FILE_APPEND);

    $successful_mailing_request = new MailingRequest($_GET["request_id"]);
    dbg('successful_mailing_request', $successful_mailing_request);
    if ($successful_mailing_request->id) {
        // if (array_key_exists('ok', $data_array) && $data_array['ok'])
        $successful_mailing_request->setStatusAndSave('sent');
        // else
        //     $successful_mailing_request->setStatusAndSave(30);
    }
    dbg($successful_mailing_request->status);
}

$telegram = new Telegram();
$telegram->token = BOT_TOKEN;
$mailing_requests = (new MailingRequest())->getPending();

dbg("Mailing Requests");

foreach ($mailing_requests as $q) {
    $mailing_request = new MailingRequest($q);

    $post_msg_json = (new Post($mailing_request->post_id))->msg_json;

    $notice_message = [
        'chat_id' => $mailing_request->chat_id,
        'reply_to_message_id' => $mailing_request->message_id,
    ];

    dbg('START ^^^^^^^^^^^^^^^^^^^^^^^^^^^^', "Mailing request. Status $mailing_request->status");
    switch ($mailing_request->status) {
        case 'draft': // Draft -> Paid
            dbg('Status: draft', $mailing_request);
            break;
        // $invoice_info = IngdgApi::getInvoiceInfo($mailing_request->invoice_id);
        // $paid = false;
        // if (isset($invoice_info['invoice_info']) && isset($invoice_info['status'])) {
        //     if (isset($invoice_info['tlg_id']) && $invoice_info['tlg_id'] == $mailing_request->user_id)
        //         // todo add 'and FULLY_PAID'
        //         $paid = true;
        // }
        // if (! $paid)
        //     break;
        // $mailing_request->setStatusAndSave('paid');
        case 'paid':  // Paid - > Stored

            $stored_message_id = $telegram->storeMessage($post_msg_json, STORAGE_ID);
            if ($stored_message_id) {
                $mailing_request->setStoredAndSave($stored_message_id);
                $mailing_request->setStatusAndSave('stored');
            } else {
                break;
            }

        case 'stored':  // Stored -> Queued
            dbg('Status: stored', $mailing_request);
            $queue_id = IngdgApi::sendToMailingQueue($mailing_request, $post_msg_json);
            dbg("queue_id = $queue_id");
            if ($queue_id) {
                $mailing_request->setQueueIdAndSave($queue_id);
                $mailing_request->setStatusAndSave('queued');
            } else {
                break;
            }
        case 'queued':  // Queued -> Q_Noticed
            dbg('Status: queued', $mailing_request);
            $notice_message['text'] = 'Ваше сообщение №' . $mailing_request->post_id . ' поставлено в очередь на рассылку';
            $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
            if (! $result) {
                unset($notice_message['reply_to_message_id']);
                $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
            }

            dbg('telegram->result:', $telegram->result);
            if ($result && isset($result['ok'])) {
                $mailing_request->setStatusAndSave('q_noticed');
            } else {
                break;
            }
        case 'q_noticed':  // Q_Noticed -> Sent
            dbg('Status q_noticed', $mailing_request);
            $sent = IngdgApi::isMailingQueueEmpty($mailing_request->queue_id);
            if ($sent) {
                $mailing_request->setStatusAndSave('sent');
            } else {
                break;
            }

        // todo test
        case 'sent':  // Sent -> commit payment    // ? or release hold
            dbg('Status sent', $mailing_request);
            if (IngdgApi::commitPayment($mailing_request->invoice_id)) {
                $mailing_request->setStatusAndSave('committed');
            } else {
                break;
            }

        case 'committed':  // Committed -> Noticed
            dbg('6', $mailing_request);
            $notice_message['text'] = 'Ваше сообщение №' . $mailing_request->post_id . ' успешно разослано';
            $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
            if (! $result) {
                unset($notice_message['reply_to_message_id']);
                $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
            }
            if ($result && isset($result['ok'])) {
                $mailing_request->setStatusAndSave('finished');
            } else {
                break;
            }
            break;

        // case 30:
        // case 'error':  // error -> error_noticed
        //     dbg('Status error', $mailing_request);
        //     $notice_message['text'] = 'Возникла ошибка при рассылке сообщения №' . $mailing_request->post_id;
        //     $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
        //     if (! $result) {
        //         unset($notice_message['reply_to_message_id']);
        //         $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
        //     }
        //     if ($result && isset($result['ok'])) {
        //         $mailing_request->setStatusAndSave(error_noticed);
        //     } else {
        //         break;
        //     }
        // case 31:
        // case 'error_noticed':  // error_noticed -> release hold
        //     dbg('Status 31', $mailing_request);
        //     if (IngdgApi::releaseHold($mailing_request->invoice_id)) {
        //         $mailing_request->setStatusAndSave('error_refund');
        //     } else {
        //         break;
        //     }
        // case 32:
        // case 'error_refund':  // release hold -> release_hold_noticed
        //     dbg('Status 32', $mailing_request);
        //     $notice_message['text'] = 'Вам возвращены средства на баланс за неуспешную рассылку сообщения №' . $mailing_request->post_id;
        //     $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
        //     if (! $result) {
        //         unset($notice_message['reply_to_message_id']);
        //         $result = $telegram->sendToTelegram($notice_message, 'sendMessage');
        //     }
        //     if ($result && isset($result['ok'])) {
        //         $mailing_request->setStatusAndSave(error_refund_noticed);
        //     } else {
        //         break;
        //     }
    }
    dbg('END ^^^^^^^^^^^^^^^^^^^^^', $mailing_request);
}


function dbg()
{
    if (isset($_GET["test"])) {
        $args = func_get_args();
        foreach ($args as $arg) {
            print_r($arg);
            print("\n==============================================\n");
        }
    }
}