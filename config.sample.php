<?php

defined('_EXEC') or exit();

include('Singleton.php');
include('IngdgApi.php');

include('DataBase.php');
include('Telegram.php');
include('Message.php');
include('MailingRequest.php');
include('User.php');
include('Post.php');
include('Menu.php');
include('Render.php');

//include('Logger.php');


// Токен для доступа с вэбхуку
define('TOKEN', '....');

define('BOT_TOKEN', '....');
define('BOT_URL', 'https://..../webhook_new.php?TOKEN=' . TOKEN);
define('CALLBACK_URL', 'https://..../mailing.php?TOKEN=' . TOKEN);

// Support chat_id
define('SUPPORT_URL', 'https://t.me/...._bot');

define('STORAGE_ID', '....');

define('TG_URL', "https://api.telegram.org/bot" . BOT_TOKEN);

define('DB_HOST', 'localhost');
define('DB_NAME', '....');
define('DB_USER', '....');
define('DB_PASSWORD', '....');

define('WITHOUT_PAY', [
    '123456789',
]);

define('DISCOUNT', [
    30 => [
        '123456789',
    ]
]);
