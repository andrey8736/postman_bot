<?php

namespace Postmanbot\Traits;


defined('_EXEC') or exit();

interface ISingleton
{

    public static function getInstance();
}


abstract class Singleton implements ISingleton
{

    private static $instance = null;


    final private function __construct() {}

    final private function __clone() {}

    final private function __wakeup() {}


    final public static function getInstance() : ISingleton
    {
        self::$instance = self::$instance ?? new static();
        return self::$instance;
    }

}
