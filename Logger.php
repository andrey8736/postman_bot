<?php

defined('_EXEC') or exit();

class Logger
{
    protected $file;
    protected $fp;

    public function __construct($file)
    {
        $this->file = $file;
        $this->fp = fopen($this->file, 'a+');
    }


    public function log($message)
    {
        if (!is_string($message)) {
            $this->logPrint($message);

            return;
        }

        $log = '';

        $log .= '[' . date('D M d H:i:s Y', time()) . '] ';
        if (func_num_args() > 1) {
            $params = func_get_args();

            $message = call_user_func_array('sprintf', $params);
        }

        $log .= $message;
        $log .= "\n";

        $this->_write($log);
    }

    public function logPrint($obj)
    {
        ob_start();

        print_r($obj);

        $ob = ob_get_clean();
        $this->log($ob);
    }

    protected function _write($string)
    {
        fwrite($this->fp, $string);

        //echo $string;
    }

    public function __destruct()
    {
        fclose($this->fp);
    }
}
