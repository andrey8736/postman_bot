<?php

define('_EXEC', 'set_web_hook');

include("config.php");


$response = '';

print("\nsetWebhook:");

try {
    $response = json_decode(
        file_get_contents(TG_URL . "/setWebhook?url=" . BOT_URL),
        $assoc = true
    );
} catch (Exception $e) {
    $response = 'Caught exception: ' . $e->getMessage();
}

print_r($response);

print("\nStatus:");
try {
    $response = json_decode(
        file_get_contents(TG_URL . "/getWebhookInfo"),
        $assoc = true
    );
    $response = $response['result'];
} catch (Exception $e) {
    $response = 'Caught exception: ' . $e->getMessage();
}

print_r($response);
