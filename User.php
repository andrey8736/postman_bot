<?php

namespace PostmanBot;


defined('_EXEC') or exit();

use PostmanBot\Post;
use PostmanBot\MailingRequest;
use PostmanBot\IngdgApi;

class User extends BaseModel
{
    public $id = null;
    public $tlg_id = null;
    public $user_data = '';
    public $refer_id = '';
    public $indt = '';
    public $per_page = 3;


    public function __construct($tlg_id, $user_data = '', $referral_id = null)
    {
        parent::__construct();

        $this->tlg_id = $tlg_id;
        $this->user_data = $user_data;
        $this->refer_id = $referral_id;

        //if (! $this->getUserByTlgId($tlg_id)) $this->save();

        $this->id = IngdgApi::getUserId($this->tlg_id, $this->user_data, $this->refer_id);

    }


    //
    // private function getUser($sql, $params)
    // {
    //     $sth = $this->db->prepare($sql);
    //     $sth->execute($params);
    //
    //     while ($row = $sth->fetch(\PDO::FETCH_ASSOC)) {
    //         $this->id = $row['id'];
    //         $this->tlg_id = $row['tlg_id'];
    //         $this->user_data = $row['user_data'];
    //         $this->refer_id = $row['refer_id'];
    //         $this->indt = $row['indt'];
    //     }
    //
    //     return $this ? $this->id : null;
    // }

    //
    // private function getUserByTlgId($tlg_id)
    // {
    //     $sql = 'SELECT * FROM `' . $this->tablePrefix('users') . '` WHERE `' . $this->tablePrefix('users') . '`.`tlg_id` = :tlg_id LIMIT 1';
    //     $params = [
    //         'tlg_id' => $tlg_id,
    //     ];
    //
    //     return $this->getUser($sql, $params);
    // }


    // private function getUserById($id)
    // {
    //     $sql = 'SELECT * FROM `' . $this->tablePrefix('users') . '` WHERE `' . $this->tablePrefix('users') . '`.`id` = :id LIMIT 1';
    //     $params = [
    //         'id' => $id,
    //     ];
    //
    //     return $this->getUser($sql, $params);
    // }


    // public function save()
    // {
    //     $params = [
    //         'tlg_id' => $this->tlg_id,
    //         'user_data' => $this->user_data,
    //         'refer_id' => $this->refer_id,
    //     ];
    //
    //     if ($this->id) {
    //         $params['id'] = $this->id;
    //
    //         $sql = 'UPDATE `' . $this->tablePrefix('users') . '`
    //                     SET `tlg_id` = :tlg_id,
    //                         `user_data` = :user_data,
    //                         `refer_id` = :refer_id
    //                     where `' . $this->tablePrefix('users') . '`.`id` = :id';
    //     } else {
    //         $sql = 'INSERT INTO `' . $this->tablePrefix('users') . '` (tlg_id, user_data, refer_id) VALUES(:tlg_id, :user_data, :refer_id)';
    //     }
    //
    //     $sth = $this->db->prepare($sql);
    //     if ($sth->execute($params)) {
    //         $this->getUserByTlgId($this->tlg_id);
    //         return $this->id;
    //     }
    //
    //     return null;
    // }

    // public function saveUser()
    // {
    //     return $this->save();
    // }

    public function getTotalPosts()
    {
        $sql = 'SELECT count(*) AS `total` FROM `' . $this->tablePrefix('msg_list')
            . '` WHERE `' . $this->tablePrefix('msg_list') . '`.`user_id` = :user_id';

        $sth = $this->db->prepare($sql);
        $sth->bindValue(':user_id', (int)$this->tlg_id, \PDO::PARAM_INT);
        $sth->execute();

        return $sth->fetch(\PDO::FETCH_ASSOC)['total'];

    }


    public function getPosts($page = 1)
    {
        $sql = 'SELECT * FROM (
                    SELECT * FROM `' . $this->tablePrefix('msg_list')
            . '` WHERE `' . $this->tablePrefix('msg_list') . '`.`user_id` = :user_id ORDER BY `id` DESC LIMIT :start, :per_page
                ) AS `tbl`  ORDER BY `id` ASC
        ';

        $sth = $this->db->prepare($sql);
        $sth->bindValue(':user_id', (int)$this->tlg_id, \PDO::PARAM_INT);
        $sth->bindValue(':per_page', (int)$this->per_page, \PDO::PARAM_INT);
        $sth->bindValue(':start', (int)$this->per_page * ($page - 1), \PDO::PARAM_INT);
        $sth->execute();

        $rows = [];

        while ($row = $sth->fetch(\PDO::FETCH_ASSOC)) {
            $rows[] = new Post(
                $row['id'],
                $row['user_id'],
                $row['msg'],
                $row['status'],
                $row['ctime'],
                $row['stime']
            );
        }
        return $rows;
    }


    public function getBalance()
    {
        return IngdgApi::getUserBalance($this->tlg_id);
    }


    public function getTotalMailingRequests($post_id = null)
    {
        $sql = "SELECT count(*) FROM `" . $this->tablePrefix('mailing_requests') . "` WHERE
                    `" . $this->tablePrefix('mailing_requests') . "`.`user_id` = :user_id
                    AND `" . $this->tablePrefix('mailing_requests') . "`.`status` not in('unknown', '0', 'draft')";

        if ($post_id)
            $sql .= ' AND `' . $this->tablePrefix('mailing_requests') . '`.`post_id` = :post_id';

        $sth = $this->db->prepare($sql);

        $sth->bindValue(':user_id', (int)$this->tlg_id, \PDO::PARAM_INT);
        if ($post_id)
            $sth->bindValue(':post_id', (int)$post_id, \PDO::PARAM_INT);
        $sth->execute();

        return $sth->fetch(\PDO::FETCH_ASSOC)['count(*)'];
    }


    public function getAllPostStatuses($post_id = null)
    {
        if ($post_id)
            $sql = "SELECT `post_id`, `status`, COUNT(*) as `total` 
                        FROM `" . $this->tablePrefix('mailing_requests') . "`
                        WHERE `" . $this->tablePrefix('mailing_requests') . "`.`user_id` = :user_id AND `post_id` = :post_id
                        GROUP BY `post_id`, `status`";
        else
            $sql = "SELECT `post_id`, `status`, COUNT(*) as `total` 
                        FROM `" . $this->tablePrefix('mailing_requests') . "`
                        WHERE `" . $this->tablePrefix('mailing_requests') . "`.`user_id` = :user_id
                        GROUP BY `post_id`, `status`";

        $sth = $this->db->prepare($sql);
        $sth->bindValue(':user_id', (int)$this->tlg_id, \PDO::PARAM_INT);
        if ($post_id)
            $sth->bindValue(':post_id', (int)$post_id, \PDO::PARAM_INT);
        $sth->execute();

        $rows = [];

        while ($row = $sth->fetch(\PDO::FETCH_ASSOC)) {
            $rows[$row['post_id']][$row['status']] = $row['total'];
        }
        return $rows;
    }


    public function getMailingRequests($post_id = null, $page = 1)
    {
        $sql = 'SELECT * FROM (
                    SELECT * FROM `' . $this->tablePrefix('mailing_requests') . '` WHERE
                               `' . $this->tablePrefix('mailing_requests') . '`.`user_id` = :user_id';
        if ($post_id)
            $sql .= ' AND `' . $this->tablePrefix('mailing_requests') . '`.`post_id` = :post_id';

        $sql .= ' ORDER BY `id` DESC LIMIT :start, :per_page ) AS `tbl`  ORDER BY `id` ASC';

        $sth = $this->db->prepare($sql);
        $sth->bindValue(':user_id', (int)$this->tlg_id, \PDO::PARAM_INT);
        if ($post_id)
            $sth->bindValue(':post_id', (int)$post_id, \PDO::PARAM_INT);
        $sth->bindValue(':per_page', (int)$this->per_page, \PDO::PARAM_INT);
        $sth->bindValue(':start', (int)$this->per_page * ($page - 1), \PDO::PARAM_INT);
        $sth->execute();

        $rows = [];

        while ($row = $sth->fetch(\PDO::FETCH_ASSOC)) {
            $rows[] = new MailingRequest($row);
        }
        return ($rows);
    }


    // public function getDiscount($price=0)
    // {
    //     $discount = 0;
    //
    //     foreach ($DISCOUNT as $d => $id_array) {
    //         if(in_array($this->tlg_id, $id_array)) {
    //             $discount = $d;
    //         }
    //     }
    //     if ($discount > 100)
    //         $discount = 100;
    //     if ($discount < 0)
    //         $discount = 0;
    //     if ($price) {
    //         return $price - $price * $discount;
    //     }
    //     return  $discount;
    // }
}
